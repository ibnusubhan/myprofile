<?php

include "../config/helper.php";
include "../config/connection.php";

$query = mysqli_query($conn, "SELECT * FROM users");
$query = mysqli_fetch_array($query);
//var_dump($query); die();
?>

<!DOCTYPE html>

<head>

    <title>Login Page</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
    <div class="frm">
        <form action="connect.php" method="POST">
            <h1>Login Mahasiswa</h1>

            <p>
                <label for="user">Username</label>
                <input type="text" id="user" name="user" />
            </p>

            <p>
                <label for="password">Password</label>
                <input type="password" id="password" name="password" />
            </p>

            <p>
                <input type="submit" class="btn" value="Login" />
            </p>
        </form>
    </div>

</body>

</html>