<?php
include "config/helper.php";
include "config/connection.php";
include "config/functions.php";

$data_mahasiswa = view("SELECT * FROM users");
$name_profile = "Ibnu Subhan";
// echo json_encode($data_mahasiswa);
// die();
?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Biodata Mahasiswa</title>
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Hind&display=swap" rel="stylesheet">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">

</head>

<body>

  <div class="testimonial-section">
    <div class="inner-width">
      <h1>Profile with me</h1>
    </div>
  </div>


  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><?= $name_profile ?></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="#about"> About<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="https://drive.google.com/open?id=0B3_kV7PoVAQmMnVmSnJHcGptWlJ2ZWRHdGdMZkk3enVkZHhB" target="_blank">Portfolio</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#contact">Contact</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            hoby
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="#">Film</a>
            <a class="dropdown-item" href="#">Basket</a>
            <a class="dropdown-item" href="#">Music</a>
          </div>
        </li>
      </ul>
    </div>
  </nav>

  <div class="side">
    <a href="gallerys/gallerys.html" target="_blank" style="background:#6ab04c;">MY GALLERYS</a>
    <a href="skill/skill.html" target="_blank" style="background:#f0932b;">Skills</a>
    <a href="#" style="background:#130f40;">Achievement</a>



  </div>


  <div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
      <img src="<?php BASE_URL; ?>assets/img/gb1.jpg" width="200" class="image-circle mb-5">
      <h2 class="display-4"><?= $data_mahasiswa[0]["name"] ?></h2>
      <p class="lead">Selamat Datang di Sistem Informasi UPJ.</p>




    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col text-center">

        <div class="social-buttons mb-5">
          <a href="https://www.facebook.com/isacz.caffello" target="_blank"><i class="fab fa-facebook-f"></i></a>
          <a href="https://twitter.com/IsaczSubhan/media" target="_blank"><i class="fab fa-twitter"></i></a>
          <a href="https://www.instagram.com/subhan_isacz/" target="_blank"><i class="fab fa-instagram"></i></a>
          <a href="https://www.youtube.com/channel/UCWkvI2FQyyyZe8JNrd-KMRw?view_as=subscriber" target="_blank"><i class="fab fa-youtube"></i></a>
          <a href="https://www.linkedin.com/in/muhamad-ibnu-subhan-isacz-3ba78516b/" target="_blank"><i class="fab fa-linkedin-in"></i></a>
        </div>

        <h1 id="about" class="mb-5">About</h1>
      </div>

    </div>
    <div class="row">
      <div class="col text-center">
        <p><?= $data_mahasiswa[0]["about"] ?>
        </p>
      </div>
    </div>
  </div>



  <div class="section-top text-center">
    <h1>Darkcode</h1>
    <a href="#">Sign Up Now</a>
    <br>
    <a href="<?= BASE_URL; ?>">Login</a>
  </div>
  <div class="section-bottom">

    <div class="contact-info">
      <div class="card">
        <i class="card-icon far fa-user"></i>
        <p id="contact">Contact Us</p>

      </div>

      <div class="card">
        <i class="card-icon far fa-envelope"></i>
        <p>ibnusubhan27@gmail.com</p>
      </div>

      <div class="card">
        <i class="card-icon fas fa-phone"></i>
        <p>Klik No WA<a href="https://wa.me/6282113157905">+6282113157905</a></p>
      </div>

      <div class="card">
        <i class="card-icon fas fa-map-marker-alt"></i>
        <p><a href="https://www.google.com/maps/place/Griya+Bintaro+Estate/@-6.2921183,106.7275127,17z/data=!3m1!4b1!4m12!1m6!3m5!1s0x2e69f006071267c3:0x9dc421477b38d263!2sGriya+Bintaro+Estate!8m2!3d-6.2921236!4d106.7297067!3m4!1s0x2e69f006071267c3:0x9dc421477b38d263!8m2!3d-6.2921236!4d106.7297067" target="_blank">Griya Bintaro</a></p>
      </div>
    </div>

  </div>
  <p>

  </p>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


  <script type="text/javascript">
    $('.testimonial-pics img').click(function() {
      $(".testimonial-pics img").removeClass("active");
      $(this).addClass("active")

      $(".testimonial").removeClass("active");
      $("#" + $(this).attr("alt")).addClass("active");

    });
  </script>

</body>

</html>