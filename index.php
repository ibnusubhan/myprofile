<?php
include "config/helper.php";
include "config/connection.php";
include "config/functions.php";

if (isset($_POST["submit"])) {

    $username = $_POST["username"];
    $password = $_POST["password"];

    $login_validation = mysqli_query($conn, "SELECT * FROM users WHERE username='" . $username . "' AND password='" . $password . "'");

    // var_dump($login_validation);
    // die();

    $check_login = mysqli_num_rows($login_validation);

    // var_dump($check_login);
    // die();

    if ($check_login > 0) {
        header("Location: index1.php");
    } else {
        header("Location: index.php");
    }
    die();
}
// echo json_encode($data_mahasiswa);
// die();
?>

<!DOCTYPE html>
<html>

<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="<?php BASE_URL; ?>assets/template/dist/css/bootstrap.min.css" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="<?php BASE_URL; ?>assets/css/customstyle.css" media="screen,projection" />

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>

    <div class="container-fluid">
        <div class="col-md-auto">
            <div class="card">
                <div class="card-body">
                    <form action="" method="POST">
                        <div class="form-group">
                            <h3 class="text-center mb-4">Login Mahasiswa</h3>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Username</label>
                            <input type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" name="password" class="form-control" id="exampleInputPassword1">
                        </div>
                        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>

        <!--Import jQuery before materialize.js-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="<?php BASE_URL; ?>assets/template/dist/css/js/bootstrap.min.js"></script>
</body>

</html>